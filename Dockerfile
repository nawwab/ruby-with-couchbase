FROM ruby:2.6.3-alpine3.9

# Minimum requirements to run a Rails app
RUN apk add --no-cache --update build-base linux-headers git postgresql-dev nodejs yarn tzdata openssh shared-mime-info cmake

CMD ["/bin/sh"]
